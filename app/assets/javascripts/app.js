$(function() {
    $(document).on('click', '.new-deploy', function() {
        $('.form-deploy').hide('fast');
        $('#tr-' + $(this).data('target')).toggle('slow');
    });

    if ( $('#console-output').length > 0 ) {
        var el = $('#console-output')[0];
        setInterval(function() {el.contentWindow.scrollTo(0,$(el.contentWindow).height());}, 500);
    }


});

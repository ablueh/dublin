class ApplicationsController < ApplicationController
  include ActionController::Live
  before_action :set_application, only: [:show, :deploy, :edit, :update]

  # GET /applications/1/edit
  def edit
  end

  def show
  end

  # PATCH/PUT /applications/1
  # PATCH/PUT /applications/1.json
  def update
    uploaded_io = params[:application][:package]

    dest = File.dirname(Rails.root.join('public', 'deployments', "#{@application.id}", uploaded_io.original_filename))
    unless File.directory?(dest)
      FileUtils.mkdir_p(dest)
    end
    File.open(File.join(dest, uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end

    @application.package = uploaded_io.original_filename
    @application.project = params[:application][:project]

    respond_to do |format|
      if @application.save
        format.html { redirect_to @application, notice: 'Application was successfully updated.' }
        format.json { render :show, status: :ok, location: @application }
      else
        format.html { render :edit }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  def deploy
    response.headers['Content-Type'] = 'text/event-stream'

    domain_home = "/opt/www/claro/#{@application.server.name.downcase}"
    deployments_root = "#{domain_home}/packages"

    from = Rails.root.join('public', 'deployments', "#{@application.id}", @application.package)
    to = File.join(deployments_root, @application.package_name)
    response.stream.write "Copying file #{from} to #{to}...\n"
    FileUtils.cp from, to
    response.stream.write "Done\n"

    script = Rails.root.join('bin', 'redeploy.py')

    url = "t3://#{@application.server.ip}:#{@application.server.port}"

    cmd = %(export MW_HOME=/opt/weblogic12; . $MW_HOME/wlserver/server/bin/setWLSEnv.sh; java weblogic.WLST #{script} #{@application.deployed_name} #{domain_home} #{url} 2>&1)

    response.stream.write "\n\nExecuting: #{cmd}\n\n"
  
    IO.popen(cmd).each do |line|
      #"Application with status completed"
      response.stream.write line
    end.close 

  ensure
    response.stream.close
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def application_params
      params[:application].permit(:package, :project)
    end
end

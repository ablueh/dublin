class ServersController < ApplicationController
  def index
    @servers = Server.all
  end
  
  def restart
    response.headers['Content-Type'] = 'text/event-stream'
    cmd = "sudo service weblogic-qa restart 2>&1"
    response.stream.write "\n\nExecuting: #{cmd}\n\n"
    #IO.popen("chuta 2>&1").each do |line|
    IO.popen(cmd).each do |line|
      #"Application with status completed"
      response.stream.write line
    end.close
    
    ensure
      response.stream.close
  end
  
  def show_restart
  end

  def rebuild_tunnels 
    response.headers['Content-Type'] = 'text/event-stream'
    
    cmd = "sudo service claro-tunnels restart 2>&1"
    response.stream.write "\n\nExecuting: #{cmd}\n\n"
    IO.popen(cmd).each do |line|
      #"Application with status completed"
      response.stream.write line
    end.close
    
    ensure
      response.stream.close
  end

  def show_rebuild_tunnels
  end

  def console_log 
    response.headers['Content-Type'] = 'text/event-stream'
    
    cmd = "tail -f /var/log/qa/STDOUT.log"
    response.stream.write "\n\nExecuting: #{cmd}\n\n"
    IO.popen(cmd).each do |line|
      #"Application with status completed"
      response.stream.write line
    end.close
    
    ensure
      response.stream.close
  end

  def show_console_log
  end
end

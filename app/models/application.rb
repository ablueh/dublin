class Application < ActiveRecord::Base
  belongs_to :server

  def url
    "http://#{server.ip}:#{server.port}#{self.path}"
  end

  def package_name
    case self.application_type
    when 'WA'
      return 'Ativacao.ear'
    when 'WM'
      return 'ClaroAtivacao.ear'
    when 'WV'
      return 'WebVendas.war'
    when 'WADMIN'
      return 'ClaroAdministrador.ear'
    end
  end

  def deployed_name
    case self.application_type
    when 'WA'
      return "Ativacao"
    when 'WM'
      return "ClaroAtivacao"
    when 'WV'
      return "WebVendas"
    when 'WADMIN'
      return "ClaroAdministrador"
    end
  end

end

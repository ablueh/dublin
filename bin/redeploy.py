import sys

app_name=sys.argv[1]

domain_home=sys.argv[2] #'/home/alexandre/proj/claro/wawm/env/wl'

url=sys.argv[3]

print "Loading domain " + domain_home + "\n"
readDomain(domain_home)

print "Connecting to " + url + "...\n"
connect(userConfigFile=domain_home+'/usercfg.properties', userKeyFile=domain_home+'/userkey.properties', url=url)

print "Deploying " + app_name + "\n"
redeploy(app_name)

print "Done\n\n>>>>>>>>\n"


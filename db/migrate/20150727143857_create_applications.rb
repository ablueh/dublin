class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :name
      t.string :path
      t.references :server, index: true, foreign_key: true
      t.string :package
      t.string :project
      t.string :application_type

      t.timestamps null: false
    end
  end
end

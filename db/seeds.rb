# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

qa1 = Server.create!(name: 'QA1', ip: '10.100.13.182', port: '5001')

Application.create([
  {name: 'Web Activation', path: '/ativacao', application_type: 'WA', server: qa1},
  {name: 'Web Migration', path: '/migracao', application_type: 'WM', server: qa1},
  {name: 'Web Admin', path: '/ClaroAdministradorWeb', application_type: 'WADMIN', server: qa1},
  {name: 'Web Vendas', path: '/WebVendas', application_type: 'WV', server: qa1}
])

qa2 = Server.create(name: 'QA2', ip: '10.100.13.182', port: '5002')

Application.create([
  {name: 'Web Activation', path: '/ativacao', application_type: 'WA', server: qa2},
  {name: 'Web Migration', path: '/migracao', application_type: 'WM', server: qa2},
  {name: 'Web Admin', path: '/ClaroAdministradorWeb', application_type: 'WADMIN', server: qa2},
  {name: 'Web Vendas', path: '/WebVendas', application_type: 'WV', server: qa2}
])

